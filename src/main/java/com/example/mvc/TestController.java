package com.example.mvc;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
	
	@RequestMapping("/invokeTest")
	public String invokeController() {
		return "Invoked controller method. Version 2";
	}
}
