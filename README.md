# Using Docker in applications #

Let's briefly look at what Docker is. In my opinion Docker can be **life and pain saver especially in big companies**, because it speeds up moving of testable builds to test department or other people dying to see the results from developers. I see it in company where I work where this process is extremely slow and painful because there are a lot of people involved. Let's take a look at basic terms and principles.

## Basic principle ##

Docker wraps any code or system in something called **Docker Image**. This docker image then can be run in something called **Docker Container**. Look at Docker Image and Docker Container in the same way as at **Java Class and Java Object**. Docker Image is similar to class where Docker Container is similar to Java object, because it's an instance of running Docker Image. Developers can share docker images in something called **Docker Registry**. We've got public docker hub registry which can be for example connected to specific branch at bitbucket.

## Summary: Important Docker terms ##

### Docker Image ###
Docker image can contain pretty much anything from web container like Tomcat having your WAR or EAR packed inside to Ubuntu system.  

### Docker Container ###
Running instance of Docker Image.

### Dockerfile ###
Build script which can be used to build Docker Image.

### Docker registry ###
Server where you pull or push your Docker Images. You can create your private Docker registry in your company or you can use public Docker registries like Docker Hub. Which is cloud based docker registry, which you can integrate with your branches at BitBucket or GitHub...Sounds cool, doesn't it?

# Howto test and install Docker #

Simply follow this excellent tutorial here:

[Howto Use Docker on OS X](https://www.viget.com/articles/how-to-use-docker-on-os-x-the-missing-guide)

Just shortly, you cannot run docker natively at OSx or Windows. You need something called **boot2docker**. Which is an simulator of LINUX machine designated to run Docker. Installation is mentioned in the tutorial, but to install VirtualBox I used following commands:


```
brew tap caskroom/cask
brew install brew-cask
brew cask install virtualbox
```

# Using Docker with simple Spring Application #

Let's build simple WAR containing easy MVC application returning just simple string. Like this:


```
package com.example.mvc;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
	
	@RequestMapping("/invokeTest")
	public String invokeController() {
		return "Invoked controller method";
	}
}
```
Super stupid I know, but we are testing Docker, not Spring...;) Now do you remember basic Docker terms? First we need Dockerfile to build Docker Image. For example we want to run our WAR inside of Tomcat. **So we will build image of Tomcat having our WAR copied inside**. Next we need to run this image inside of Docker Container.

## Building of Dockerfile ##


```
# Pull base image from docker.io registry, which is default registry.
From tomcat:8-jre8

# Maintainer...yes, it's me.
MAINTAINER "Tomas Kloucek <kloucektomas@yahoo.com">

# Let's copy our WAR to our newly created Tomcat image
ADD ./target/demo-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/
```

## Building the Docker Image ##

Run following command:

```
docker build -t demodocker .
```
Where demodocker is the name of our image and '.' is the path to previously mentioned Dockerfile.

## Running the Tomcat Docker Image inside of Docker Container ##

Run following command:

```
docker run -it --rm -p 8080:8080 --name demo-0.0.1-SNAPSHOT demodocker
```

* docker run -> We're launching docker container
* -it -> Means interactive and terminal mode, we will see how container is running.
* -p -> Port mapping from VM to our system.
* --name demo-0.0.1-SNAPSHOT -> Name of container.
* demodocker -> Name of Docker Imager.

Next, let's figure out IP of our boot2docker LINUX simulator...

```
boot2docker ip
```

Now we can test our WAR in browser, type following URL:

```
http://<IP of boot2docker>:8080/demo-0.0.1-SNAPSHOT/invokeTest
```

That's all. Take this short tutorial as an entry into the Docker world. Guys, give Docker a try, it's worth it!